package org.example.controller;

import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.zeroturnaround.exec.ProcessExecutor;
import org.zeroturnaround.exec.ProcessOutput;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * @author Aleksey Buzanov
 */

@RestController
@RequestMapping(value = "/java")
@Slf4j
public class ClassExecutorController {

    private String compileFile(final File file,
                               final InputStream inputStream,
                               final String[] args) throws Exception {
        log.debug("compileFile({})", file.getName());
        final List<String> compileCommand = Arrays.asList("cmd", "/c",
                "\"" + System.getProperty("java.home").concat("\\bin\\javac.exe\" ")
                        + file.getPath());

        log.debug("Compile command: {}", compileCommand);

        List<String> execCommand = new ArrayList<String>(Arrays.asList("cmd", "/c"
                , "java", "-classpath", file.getParent()
                , file.getName().replace(".java", "")));
        execCommand.addAll(Arrays.asList(args));

        log.debug("Exec command: {}", execCommand);

        final String compileOutput = new ProcessExecutor()
                .timeout(10000, TimeUnit.MILLISECONDS)
                .command(compileCommand)
                .readOutput(true)
                .execute()
                .outputString();

        log.info("Compile output: " + compileOutput);

        final String execOutput = new ProcessExecutor()
                .timeout(10000, TimeUnit.MILLISECONDS)
                .command(execCommand)
                .redirectInput(inputStream)
                .readOutput(true)
                .execute()
                .outputString();

        log.debug("Exec output: " + execOutput);
        return compileOutput.concat("\n").concat(execOutput);
    }

    @PostMapping(value = "/compile", consumes = MediaType.TEXT_PLAIN_VALUE)
    public String compileString(@RequestHeader("args") String[] args,
                                @RequestHeader("input") String[] input,
//                                @RequestHeader("classname") String classname,
                                @RequestBody String code) throws Exception {

        if (code == null)
            throw new NullPointerException("Файл пустой");

        String classname = "";
        String[] s1 = code.split(" ");
        for (int i = 0; i < s1.length; i++) {
            if ("class".equals(s1[i]) && i + 1 < s1.length) {
                classname = s1[i + 1];
                break;
            }
        }
        if (classname.isEmpty()) {
            throw new NullPointerException("Нет объявления класса в коде");
        }

        final File file = new File(System.getProperty("java.io.tmpdir") +
                System.getProperty("file.separator") +
                UUID.randomUUID().toString() +
                System.getProperty("file.separator") +
                classname.concat(".java"));
        FileUtils.forceMkdirParent(file);
        if (!file.exists())
            file.createNewFile();
        try (BufferedOutputStream stream = new BufferedOutputStream
                (new FileOutputStream(file))) {
            stream.write(code.getBytes());
        }
        final String str = Arrays
                .stream(input)
                .map(s -> s.concat(System.lineSeparator()))
                .collect(Collectors.joining());
        InputStream is = new ByteArrayInputStream(str.getBytes());
        String output = compileFile(file, is, args);
        FileUtils.deleteDirectory(file.getParentFile());
        return output;
    }

//    @PostMapping(value = "/loadMainClass")
//    public ResponseEntity<String> loadMainClass(MultipartHttpServletRequest data) throws Exception {
//        final MultipartFile mfile = data.getFile("file");
//        if (mfile == null)
//            throw new NullPointerException("Файл пустой");
//        byte[] bytes = mfile.getBytes();
//
//        final java.lang.ClassLoader classLoader = ClassLoader.getSystemClassLoader();
//
//        final Field theUnsafeField = Unsafe.class.getDeclaredField("theUnsafe");
//        theUnsafeField.setAccessible(true);
//        final Unsafe unsafe = (Unsafe) theUnsafeField.get(null);
//        final Class<?> myCustomInvocator = unsafe.defineClass(null, bytes, 0,
//                bytes.length, classLoader, null);
//
//        Reflection.runMainInSameVM(myCustomInvocator, null);
//
//        return ResponseEntity.ok("Выполнено успешно");
//    }
}
